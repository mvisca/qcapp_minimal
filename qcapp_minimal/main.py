import sys
from PyQt5 import QtWidgets
sys.path.append("./src")
import main_windows


def run():
    print("Executing...")
    app = QtWidgets.QApplication(sys.argv)
    windows = main_windows.MainWindows()
    windows.show()
    sys.exit(app.exec_())



