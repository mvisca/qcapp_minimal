import configtools
import gc
import loc_uis
from PyQt5 import QtWidgets, uic

Ui_meta, _ = uic.loadUiType(loc_uis.get_path_to("metadata.ui", current=__file__))

class MetadataWindows(QtWidgets.QMainWindow, Ui_meta):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_meta.__init__(self)
        self.setupUi(self)
        self.cfg_file = None
        self.setParent(None)
        self.save_btn.clicked.connect(self._save)

    def read_config(self):
        self.cfg_file = configtools.ConfigFile()

    def _save(self):
        self.read_config()
        _to_save = {
            "latitude": self.latitude.text(),
            "longitude": self.longitude.text(),
            "altitude": self.altitude.text(),
            "year": self.year.text(),
        }
        with self.cfg_file:
            self.cfg_file.update_section("METADATA", _to_save)
        #msgBox = QtWidgets.QMessageBox.about(self, "Message", "Done.")
        self.close()

    def closeEvent(self, event):
        self.close()
        gc.collect()
        event.accept()
