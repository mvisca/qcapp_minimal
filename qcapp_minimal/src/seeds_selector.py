import gc
import sys
import numpy
import configtools
import loc_uis
from gompertz import gompertz_fun
from PyQt5 import QtWidgets, uic
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

UI_seeds, _ = uic.loadUiType(loc_uis.get_path_to("SeedsPreview.ui", current=__file__))

class SeedsSelector(QtWidgets.QMainWindow, UI_seeds):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        UI_seeds.__init__(self)
        self.setupUi(self)
        self.id = None
        self.setParent(None)
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self.frame)
        self.canvas_layout.addWidget(self.toolbar)
        self.canvas_layout.addWidget(self.canvas)
        self.ax = self.figure.add_subplot(111)
        self.ax.axis([0, 1, 0, 1])
        self.ax.set_xlabel("kt")
        self.ax.set_ylabel("kn")
        self.ax.grid("on")
        self.curves = dict()
        self.plot_empty_data()
        self.plot_curves()

        self.upper_a.valueChanged.connect(lambda: self.update_curve("upper"))
        self.upper_b.valueChanged.connect(lambda: self.update_curve("upper"))
        self.upper_c.valueChanged.connect(lambda: self.update_curve("upper"))

        self.mid_a.valueChanged.connect(lambda: self.update_curve("mid"))
        self.mid_b.valueChanged.connect(lambda: self.update_curve("mid"))
        self.mid_c.valueChanged.connect(lambda: self.update_curve("mid"))

        self.lower_a.valueChanged.connect(lambda: self.update_curve("lower"))
        self.lower_b.valueChanged.connect(lambda: self.update_curve("lower"))
        self.lower_c.valueChanged.connect(lambda: self.update_curve("lower"))

        self.save_btn.clicked.connect(self.save_seeds)

    def set_id(self, _id):
        self.id = _id

    def get_seeds(self):
        seeds = list()
        seeds.extend(self.get_upper_seeds())
        seeds.extend(self.get_mid_seeds())
        seeds.extend(self.get_lower_seeds())
        return seeds

    def get_upper_seeds(self):
        return [
            self.upper_a.value(),
            self.upper_b.value(),
            self.upper_c.value()
        ]

    def get_mid_seeds(self):
        return [
            self.mid_a.value(),
            self.mid_b.value(),
            self.mid_c.value()
        ]

    def get_lower_seeds(self):
        return [
            self.lower_a.value(),
            self.lower_b.value(),
            self.lower_c.value()
        ]

    def plot_curves(self):
        x = numpy.linspace(0, 1, 100)
        selector = {
            "upper": self.get_upper_seeds,
            "mid": self.get_mid_seeds,
            "lower": self.get_lower_seeds,
        }

        for curve in ["upper", "mid", "lower"]:
            y = gompertz_fun(x, selector[curve]())
            self.curves[curve] = self.ax.plot(x, y, label=curve)

        self.ax.legend(loc=2)
        self.canvas.draw()

    def update_curve(self, curve):
        selector = {
            "upper": self.get_upper_seeds,
            "mid": self.get_mid_seeds,
            "lower": self.get_lower_seeds,
        }

        handler = self.curves[curve][0]
        x = handler.get_xdata()
        seeds = selector[curve]()
        y = gompertz_fun(x, seeds)
        handler.set_ydata(y)
        self.canvas.draw()

    def plot_empty_data(self):
        self.curves["data"] = self.ax.plot(
            [], [], ".b", alpha=0.9, label="Data"
            )

    def update_data(self, new_data):
        handler = self.curves["data"][0]
        handler.set_xdata(new_data["kt"])
        handler.set_ydata(new_data["kn"])
        self.canvas.draw()

    def read_config(self):
        self.cfg_file = configtools.ConfigFile()

    def save_seeds(self):
        self.read_config()
        all_seeds = self.get_seeds()
        seeds_names = [
            "upper_a", "upper_b", "upper_c",
            "mid_a", "mid_b", "mid_c",
            "lower_a", "lower_b", "lower_c",
        ]
        to_save = dict(zip(seeds_names, map(str, all_seeds)))
        with self.cfg_file:
            self.cfg_file.update_section(f"ZONE{self.id}", to_save)

    def closeEvent(self, event):
        self.close()
        gc.collect()
        event.accept()

if __name__ == "__main__":
    #df = pandas.read_csv("data.csv")
    app = QtWidgets.QApplication(sys.argv)
    windows = SeedsSelector(1)
    sys.exit(app.exec_())
