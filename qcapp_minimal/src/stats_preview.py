import loc_uis
from PyQt5 import QtWidgets, uic
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.patches import Patch

Ui_stats, _ = uic.loadUiType(loc_uis.get_path_to("statsPreview.ui", current=__file__))


class Preview(QtWidgets.QMainWindow, Ui_stats):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_stats.__init__(self)
        self.setupUi(self)
        self.setParent(None)
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.canvas_layout.addWidget(self.toolbar)
        self.canvas_layout.addWidget(self.canvas)

        self.ax1 = self.figure.add_subplot(111)
        self.ax1.grid("on", ls="--", alpha=0.8)
        self.ax2 = self.ax1.twinx()

    def plot_bar_stats(self, data):
        self.ax2.cla()
        width = 0.35
        loc = [(x+width/2) for x in range(1, 5)]
        labels = ["Zone1", "Zone2", "Zone3", "General"]
        y = list()
        for key in data.keys():
            self.ax1.bar(
                [key, key+width], data[key][:-1],
                width, color=["b", "g"]
            )
            y.append(data[key][-1])
        self.ax1.set_ylabel("Number of Data")

        self.ax2.set_ylabel("Discard Rate (%)", color="red")
        self.ax2.tick_params(axis="y", labelcolor="red")
        self.ax2.plot(loc, y, marker="*", color="red", label="Discard rate")
        self.ax1.set_xticks(loc)
        self.ax1.set_xticklabels(labels)
        blue_patch = Patch(color="blue", label="Valid pairs (kt, kn)")
        green_patch = Patch(color="green", label="Good pairs (kt, kn)")
        self.ax1.legend(handles=[blue_patch, green_patch], loc=2)
        self.ax2.legend()
