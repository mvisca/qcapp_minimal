import gc
import sys
import time
import warnings
import numpy as np
from PyQt5 import QtWidgets, uic

import envelopes

import datatools
import loc_uis

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.path import Path

warnings.filterwarnings("ignore", module="matplotlib")

UI_main, _ = uic.loadUiType(loc_uis.get_path_to("MainWindows.ui", current=__file__))
class MainWindows(QtWidgets.QMainWindow, UI_main):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        UI_main.__init__(self)
        self.setupUi(self)
        self.setParent(None)

        self.progressBar.setEnabled(False)

        self.data = None
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel("Kt")
        self.ax.set_ylabel("Kn")
        self.ax.grid("on")

        self.canvas_layout.addWidget(self.toolbar)
        self.canvas_layout.addWidget(self.canvas)

        self.curves = dict()

        self.zone_params = {
            'all': {
                "1": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "2": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "3": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "all": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                }
            },
            'winter': {
                "1": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "2": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "3": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "all": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                }
            },
            'autumn': {
                "1": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "2": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "3": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "all": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                }
            },
            'autumn': {
                "1": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "2": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "3": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "all": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                }
            },
            'spring': {
                "1": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "2": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "3": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "all": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                }
            },
            'summer': {
                "1": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "2": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "3": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                },
                "all": {
                    "r": (0,0,0,0,0,0,0,0), "l": (0,0,0,0,0,0,0,0),
                    "cant_points": "", "points_in": "", "dis_rate": ""
                }
            }
        }

        self.load_data_btn.clicked.connect(self.load_data)

        self.open_folder.clicked.connect(lambda: self.select_dir("in"))
        self.output_folder.clicked.connect(lambda: self.select_dir("out"))

        # self.show_stats.clicked.connect(self.show_stats_windows)

        self.zones.currentIndexChanged.connect(self.update_preview)
        self.seasons.currentIndexChanged.connect(self.update_preview)

        self.r_a.valueChanged.connect(lambda: self.update_curve("r"))
        self.r_b.valueChanged.connect(lambda: self.update_curve("r"))
        self.r_c.valueChanged.connect(lambda: self.update_curve("r"))
        self.r_d.valueChanged.connect(lambda: self.update_curve("r"))
        self.r_x0.valueChanged.connect(lambda: self.update_curve("r"))
        self.r_y0.valueChanged.connect(lambda: self.update_curve("r"))

        self.l_a.valueChanged.connect(lambda: self.update_curve("l"))
        self.l_b.valueChanged.connect(lambda: self.update_curve("l"))
        self.l_c.valueChanged.connect(lambda: self.update_curve("l"))
        self.l_d.valueChanged.connect(lambda: self.update_curve("l"))
        self.l_x0.valueChanged.connect(lambda: self.update_curve("l"))
        self.l_y0.valueChanged.connect(lambda: self.update_curve("l"))

        self.kt_max.valueChanged.connect(lambda: self.update_limits("kt"))
        self.kn_max.valueChanged.connect(lambda: self.update_limits("kn"))

        self.tagButton.clicked.connect(self.make_path)

        self.export_btn.clicked.connect(self.export)
        self.disable_panel()
        self.init_plot()
        self.show()

    def disable_panel(self):
        self.r_a.setEnabled(False)
        self.r_b.setEnabled(False)
        self.r_c.setEnabled(False)
        self.r_d.setEnabled(False)
        self.l_a.setEnabled(False)
        self.l_b.setEnabled(False)
        self.l_c.setEnabled(False)
        self.l_d.setEnabled(False)
        self.kt_max.setEnabled(False)
        self.kn_max.setEnabled(False)

    def enable_panel(self):
        self.r_a.setEnabled(True)
        self.r_b.setEnabled(True)
        self.r_c.setEnabled(True)
        self.r_d.setEnabled(True)
        self.l_a.setEnabled(True)
        self.l_b.setEnabled(True)
        self.l_c.setEnabled(True)
        self.l_d.setEnabled(True)
        self.kt_max.setEnabled(True)
        self.kn_max.setEnabled(True)


    def update_preview(self):
        zone = self.zones.currentText()
        season =  self.seasons.currentText()

        self.disable_panel()
        if self.data is not None:
            if zone is not "" and self.seasons is not "":
                if season == 'all':
                    msk_season = (self.data.df['season'] == 'winter') | (self.data.df['season'] == 'autumn') | \
                        (self.data.df['season'] == 'spring') | (self.data.df['season'] == 'summer')
                else:
                    msk_season = self.data.df['season'] == season

                if zone == 'all':
                    msk_zone = self.data.df['zones'].between(1, 3)
                else:
                    msk_zone = self.data.df["zones"] == int(zone)

                msk = msk_season & msk_zone

                self.ax.legend(loc=2)
                handler = self.curves["data"][0]
                handler.set_xdata(self.data.df["kt"][msk])
                handler.set_ydata(self.data.df["kn"][msk])

                titles = {
                    "1": "Air mass in [1.00, 1.25]",
                    "2": "Air mass in (1.25, 2.50]",
                    "3": "Air mass in (2.50, 8.33]",
                    "all": "Air mass in [1.00, 8.33]",
                }

                self.ax.set_title(f"Season: {season} -- Zone: {zone}: "+ titles[zone])
                msk_in = self.data.df["inside"][msk]
                handler = self.curves["data_in"][0]
                handler.set_xdata(self.data.df["kt"][msk][msk_in])
                handler.set_ydata(self.data.df["kn"][msk][msk_in])

                self.r_a.setValue(self.zone_params[season][zone]["r"][0])
                self.r_b.setValue(self.zone_params[season][season][zone]["r"][1])
                self.r_c.setValue(self.zone_params[season][zone]["r"][2])
                self.r_d.setValue(self.zone_params[season][zone]["r"][3])

                self.l_a.setValue(self.zone_params[season][zone]["l"][0])
                self.l_b.setValue(self.zone_params[season][zone]["l"][1])
                self.l_c.setValue(self.zone_params[season][zone]["l"][2])
                self.l_d.setValue(self.zone_params[season][zone]["l"][3])

                self.kt_max.setValue(self.zone_params[season][zone]["kt_max"])
                self.kn_max.setValue(self.zone_params[season][zone]["kn_max"])

                self.cant_points.setText(self.zone_params[season][zone]["cant_points"])
                self.cant_points_in.setText(self.zone_params[season][zone]["points_in"])
                self.discard_rate.setText(self.zone_params[season][zone]["dis_rate"])

        self.canvas.draw()
        self.enable_panel()


    def update_limits(self, limit):

        zone = self.zones.currentText()
        seasons = self.seasons.currentText()
        selector = {
            "kt": self.curves["kt_max"],
            "kn": self.curves["kn_max"]
        }
        handler = selector[limit]
        if limit is "kt":
            val = self.kt_max.value()
            handler.set_xdata([val, val])
        else:
            val = self.kn_max.value()
            handler.set_ydata([val, val])

        if self.kt_max.isEnabled():
        	self.zone_params[season][zone]["kt_max"] = self.kt_max.value()
        	self.zone_params[season][zone]["kn_max"] = self.kn_max.value()

        self.canvas.draw()

    def update_curve(self, curve):
        selector = {
            "r" : self.curves["r"][0],
            "l" : self.curves["l"][0],
        }

        params = self.get_params(curve)
        handler = selector[curve]
        x = np.linspace(0, 1, 100)
        handler.set_xdata(x)
        handler.set_ydata(
            envelopes.fgompertz(x, params)
        )
        if self.r_a.isEnabled():
            zone = self.zones.currentText()
            season = self.seasons.currentText()
            self.zone_params[season][zone][curve] = params
        self.canvas.draw()

    def init_plot(self):
        self.curves["data"] = self.ax.plot([], [], ".b", alpha=0.8, ms=0.4, label="Data")
        x = np.linspace(0, 1, 100)
        self.curves["ref"] = self.ax.plot(x, x, ls="--", color="grey", alpha=0.7, label="kt=kn")
        self.curves["r"] = self.ax.plot([], [], "-r", alpha=0.9, label="Upper")
        self.curves["l"] = self.ax.plot([], [], "-y", alpha=0.9, label="Lower")
        self.curves["kt_max"] = self.ax.axvline(1, 0, 1, color="k", ls="--")
        self.curves["kn_max"] = self.ax.axhline(1, 0, 1, color="k", ls="--")
        self.curves["data_in"] = self.ax.plot([], [], ".g", alpha=0.9, ms=0.4)


    def get_params(self, curve):
        if curve is "r":
            return (
                self.r_a.value(),
                self.r_b.value(),
                self.r_c.value(),
                self.r_d.value(),
                self.r_x0.value(),
                self.r_y0.value()
            )
        else:
            return (
                self.l_a.value(),
                self.l_b.value(),
                self.l_c.value(),
                self.l_d.value(),
                self.l_x0value(),
                self.l_y0.value()
            )


    def select_dir(self, line):
        selector = {
            "in": self.path_LineEdit,
            "out": self.output_path,
        }
        obj = QtWidgets.QFileDialog.DontUseNativeDialog
        path = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self, caption="Select Directory", options=obj
        )
        selector[line].setText(path)
        self.load_data_btn.setEnabled(True)

    def export(self):
        if self.data is None:
            QtWidgets.QMessageBox.information(self, "Message", "Nothing to export, process the data first.")
        else:
            self.progressBar.setEnabled(True)
            self.output_list.clear()
            self.data.export_files_by_doy(
                path=self.output_path.text(),
                prefix=self.prefix.text(),
                progress=self.progressBar,
                display_list=self.output_list
            )
            QtWidgets.QMessageBox.information(self, "Message", "Done.")

    def load_data(self):
        path = self.path_LineEdit.text()
        self.data = datatools.Data(path+"/")
        self.prefix.setText(self.data.filename_prefix)
        QtWidgets.QMessageBox.information(self, "Message", "Done.")

    def make_path(self):
        r_curve = self.curves["r"][0]
        l_curve = self.curves["l"][0]
        x_r = r_curve.get_xdata()
        y_r = r_curve.get_ydata()

        x_l = l_curve.get_xdata()
        y_l = l_curve.get_ydata()

        zone = self.zones.currentText()
        season = self.seasons.currentText()
        kt_max = self.zone_params[season][zone]["kt_max"]
        kn_max = self.zone_params[season][zone]["kn_max"]
        params_r = self.zone_params[season][zone]["r"]
        params_l = self.zone_params[season][zone]["l"]

        msk_r = (y_r > 0) & (x_r <= 1) & (y_r <= 1)
        x_r = np.concatenate([[x_r[np.where(msk_r)][0]], x_r[np.where(msk_r)]])
        y_r = np.concatenate([[0], y_r[np.where(msk_r)]])

        msk_l = (y_l > 0) & (x_l <= 1) & (y_l <= 1)
        x_l = np.concatenate([[x_l[np.where(msk_l)][0]], x_l[np.where(msk_l)]])
        y_l = np.concatenate([[0], y_l[np.where(msk_l)]])
        x_l = np.concatenate([[x_r[0]], x_l])
        y_l = np.concatenate([[y_r[0]], y_l])

        # Make path
        verts = [(x, y) for x, y in zip(x_r, y_r)]
        verts_l = [(x, y) for x, y in zip(x_l, y_l)]
        verts.extend(reversed(verts_l))
        codes = [Path.MOVETO]
        codes.extend([Path.LINETO for i in range(len(verts) - 2)])
        codes.append(Path.CLOSEPOLY)
        self.path = Path(verts, codes)

        if season == 'all':
            msk_season = (self.data.df['season'] == 'winter') | (self.data.df['season'] == 'autumn') | \
                (self.data.df['season'] == 'spring') | (self.data.df['season'] == 'summer')
        else:
            msk_season = self.data.df['season'] == season

        if zone == 'all':
            msk_zone = self.data.df['zones'].between(1, 3)
        else:
            msk_zone = self.data.df["zones"] == int(zone)

        msk_real = msk_zone & msk_season
        points = list(zip(self.data.df["kt"][msk_real].values, self.data.df["kn"][msk_real].values))
        self.data.df.loc[msk_real, "inside"] = list(self.tag_points(points, self.path, kt_max, kn_max))


        msk_in = self.data.df["inside"][msk_real]
        handler = self.curves["data_in"][0]
        handler.set_xdata(self.data.df["kt"][msk_real][msk_in])
        handler.set_ydata(self.data.df["kn"][msk_real][msk_in])

        cant_points = np.sum(~(
            np.isnan(self.data.df["kt"][msk_real]) | \
            np.isnan(self.data.df["kn"][msk_real])
            ))
        cant_points_in = np.sum(msk_in)
        discard_rate = round(100*(cant_points_in/cant_points), 2)

        self.zone_params[season][zone]["cant_points"] = str(cant_points)
        self.zone_params[season][zone]["points_in"] = str(cant_points_in)
        self.zone_params[season][zone]["dis_rate"] = str(discard_rate)

        self.cant_points.setText(str(cant_points))
        self.cant_points_in.setText(str(cant_points_in))
        self.discard_rate.setText(str(round(discard_rate, 2)))

        self.canvas.draw()
        return self.path

    def tag_points(self, points, path, kt_max, kn_max):
        for point in points:
            if path.contains_point(point) and (point[0] <= kt_max and point[1] <= kn_max):
                yield True
            else:
                yield False


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    windows = MainWindows()
    app.exec_()
