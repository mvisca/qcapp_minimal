import numpy as np
from matplotlib.path import Path

import configtools
import time
import numba


class Envelopes(object):
    def __init__(self, _id=None):
        # Creates the curves
        self.x_coords = None
        self.id = _id
        self.sup_curve = None
        self.inf_curve = None
        self.status = dict()


    def make_path(self):
        sup_curve = self.sup_curve.function(self.x_coords)
        inf_curve = self.inf_curve.function(self.x_coords)
        inf_curve = inf_curve - inf_curve.min()  # Correction, must touch 0
        # Find points where inf_curve > sup_curve, when kt->1
        msk = (self.x_coords > 0.6) & (inf_curve > sup_curve)
        sup_curve = sup_curve[~msk]
        inf_curve = inf_curve[~msk]
        # Make path
        verts = [(x, y) for x, y in zip(self.x_coords, sup_curve)]
        verts_inf = [(x, y) for x, y in zip(self.x_coords, inf_curve)]
        verts.extend(reversed(verts_inf))
        codes = [Path.MOVETO]
        codes.extend([Path.LINETO for i in range(len(verts) - 2)])
        codes.append(Path.CLOSEPOLY)
        self.path = Path(verts, codes)
        return self.path

    def points_inside(self, data):
        points = zip(data["kt"], data["kn"])
        ret = self.path.contains_points(list(points))
        return ret

    def points_near_path(self, data, distance=0.035):
        ret = np.fromiter((is_ok2(
            x,
            self.path,
            self.sup_curve.function,
            self.inf_curve.function,
            distance
            ) for x in data), bool, len(data))

        return ret


def fgompertz(kt, parameters):
    """ Gompertz function of the paper """
    a, b, c, d, x0, y0 = parameters
    term1 = d*(kt - x0)
    term2 = c*b
    term3 = a*b

    return a*np.power(b, c*np.power(b, term1)) + y0

def rmsd(data, ref):
    return np.sqrt(np.mean(np.square(data - ref)))


def dist(point, ref):
    return np.sqrt(np.sum((point-ref)**2, axis=1))


def gpoint(x, f):
    return np.array([x, f(x)])

def is_ok2(point, path, sup, inf, d):
    if np.isnan(point).any():
        return False
    else:
        i = np.linspace(point[0]-0.1, point[0]+0.1, 50)
        d1 = dist(gpoint(i, sup).T, point)
        d2 = dist(gpoint(i, inf).T, point)

        d1 = np.min(d1)
        d2 = np.min(d2)
        if np.min((d1, d2)) < d:
            return True
        else:
            return False
