import configtools
import sys
import gc

from PyQt5 import QtWidgets, uic
import loc_uis

Ui, _ = uic.loadUiType(loc_uis.get_path_to("parameters.ui", current=__file__))


class ParametersWindows(QtWidgets.QMainWindow, Ui):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui.__init__(self)
        self.setupUi(self)
        self.setParent(None)
        self.id = None
        self.save_btn.clicked.connect(self._save)

    def set_id(self, _id):
        self.id = _id

    def _save(self):
        self.cfg_file = configtools.ConfigFile()
        _to_save = {
            "upper_max_iters": self.upper_iters.text(),
            "mid_max_iters": self.mid_iters.text(),
            "lower_max_iters": self.lower_iters.text(),
            "bins": self.bins.text(),
            "tol": self.tolerance.text(),
            "k_factor": self.k_value.text(),
        }
        with self.cfg_file:
            self.cfg_file.update_section(f"ZONE{self.id}", _to_save)
        #msgBox = QtWidgets.QMessageBox.about(self, "Message", "Done.")
        self.close()

    def closeEvent(self, event):
        self.close()
        gc.collect()
        event.accept()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    windows = ParametersWindows()
    windows.show()
    sys.exit(app.exec_())
