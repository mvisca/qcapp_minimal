import numpy as np
import numba
import time


class Gompertz(object):
    def __init__(self, parameters=[1, 10, 3]):
        self.parameters = np.array(parameters, dtype=np.float64)
        self.status = dict()

    def fit(self, xdata, ydata, max_iters=1000, tol=1e-5):
        """
        Fit the parameters of the Gompertz's function using Gauss-Newton method
        """
        converge = False
        iterations = 0
        seeds = self.parameters
        while not converge and (iterations <= max_iters):
            Jf = self.jacobian(xdata)
            y = self.function(xdata)

            A = np.dot(Jf.T, Jf)
            X = np.dot(Jf.T, (ydata - y))
            dB = np.linalg.solve(A, X)
            # dB = np.dot(np.dot(np.linalg.inv(A), Jf.T), (ydata - y))

            if np.allclose(seeds, seeds + dB, atol=tol, rtol=tol):
                converge = True
                seeds += dB
            else:
                seeds += dB
                iterations += 1
        
        self.status["converged"] = converge
        self.status["iters_done"] = iterations
        return seeds

    def function(self, x):
        """
        Return the functional value for a set of parameters of the Gompertz's \
        function in a x-value.
        """
        a, b, c = self.parameters
        return a*np.exp(-b*np.exp(-c*x))

    def jacobian(self, x):
        """
        Return the jacobian of the Gompertz's function in a given x-value in \
        in respect to a set of parameters.
        """
        a, b, c = self.parameters
        Fa = np.exp(-b*np.exp(-c*x))
        Fb = -a*np.exp(b*(-np.exp(-c*x)*(-c*x)))
        Fc = a*b*x*np.exp(b*(-np.exp(-c*x))*(-c*x))

        return np.array([Fa, Fb, Fc]).T


def gompertz_fun(x, parameters):
    """
    Return the functional value for a set of parameters of the Gompertz's \
    function in a x-value.
    """
    a, b, c = parameters
    return a*np.exp(-b*np.exp(-c*x))
